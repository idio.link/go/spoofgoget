/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package main

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net"
	"net/http"
	"os"
	"path"
	"regexp"
	"time"
)

var goGetTmpl *template.Template

func init() {
	const respTmpl = `<!doctype html>` +
		`<html>` +
		`<head>` +
		`<meta name="go-import"` +
		` content="{{.Module}} git {{.ImportURI}}">` +
		`<meta name="go-source"` +
		` content="{{.Module}}` +
		` {{print .SourceURI}}` +
		` {{.SourceURI}}/src/{{.Branch}}{/dir}` +
		` {{.SourceURI}}/src/{{.Branch}}{/dir}/{file}#L{line}">` +
		`</head>` +
		`<body>` +
		`go get https://{{.Module}}` +
		`</body>` +
		`</html>`

	goGetTmpl =
		template.Must(template.New("resp").Parse(respTmpl))
}

func NewServer(ctx context.Context) *Server {
	return &Server{ctx: ctx}
}

type Server struct {
	ctx      context.Context
	svr      *http.Server
	projects Cached[map[string]GitRepo]
}

func (s *Server) ListenAndServe(port int) error {
	s.svr = &http.Server{
		Addr:         fmt.Sprintf(":%d", port),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		BaseContext: func(_ net.Listener) context.Context {
			return s.ctx
		},
	}
	s.paths()
	return s.svr.ListenAndServe()
}

func (s *Server) Shutdown() error {
	return s.svr.Shutdown(s.ctx)
}

func (s *Server) paths() {
	http.HandleFunc("/go/", s.handleGoGet())
}

func (s *Server) handleGoGet() http.HandlerFunc {
	accessToken, ok := os.LookupEnv("GOGS_ACCESS_TOKEN")
	if !ok {
	}
	version := regexp.MustCompile(`/v[0-9]+$`)
	goSubpath := regexp.MustCompile(`/go/`)
	client := NewGogsClient(s.ctx, accessToken)

	return func(w http.ResponseWriter, req *http.Request) {
		if q := req.URL.Query(); q.Get("go-get") != "1" {
			code := http.StatusNotFound
			http.Error(w, http.StatusText(code), code)
			return
		}
		module := req.Host + path.Clean(req.URL.Path)
		module = version.ReplaceAllString(module, "")
		// example.com/go/blarg/v2 |-> example.com/go/blarg
		// example.com/go/blarg |-> example.com/go, blarg
		modPath := version.ReplaceAllString(module, "")
		modPath = goSubpath.ReplaceAllString(modPath, "/")
		group := req.Host

		// populate cache if expired
		if s.projects.Expired() {
			projects := map[string]GitRepo{}
			pager := FetchGogsOrganizationRepos(client, group)
			for p := range pager.Iter() {
				projects[p.Path()] = p
			}
			if err := pager.LastErr(); err != nil {
				code := http.StatusServiceUnavailable
				http.Error(w, http.StatusText(code), code)
				log.Printf("request go-get: unable to fetch gogs projects: %v", err)
				return
			}
			s.projects = NewCached(&projects, 30*time.Minute)
		}

		var project GitRepo

		project, ok := (*s.projects.Value())[modPath]
		if !ok {
			code := http.StatusNotFound
			http.Error(w, http.StatusText(code), code)
			log.Printf("request go-get: unable to find git project for module '%s'", module)
			return
		}
		// TODO this may write a partial result on error; don't
		err := goGetTmpl.Execute(w, struct {
			Module    string
			ImportURI string
			SourceURI string
			Branch    string
		}{module,
			project.GitURI(),
			project.WebURI(),
			project.DefaultBranch(),
		})
		if err != nil {
			code := http.StatusInternalServerError
			http.Error(w, http.StatusText(code), code)
			log.Printf("request go-get: execute template: '%s': %v", modPath, err)
		}
		return
	}
}

func NewCached[T any](
	value *T,
	lifetime time.Duration,
) Cached[T] {
	return Cached[T]{
		value:  value,
		expiry: time.Now().UTC().Add(lifetime),
	}
}

type Cached[T any] struct {
	value  *T
	expiry time.Time
}

func (c *Cached[T]) Value() *T {
	return c.value
}

func (c *Cached[T]) Expired() bool {
	return c.expiry.Before(time.Now().UTC())
}
