/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"

	"golang.org/x/sys/unix"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	srv := NewServer(ctx)

	// Catch signals
	go func() {
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, unix.SIGINT, unix.SIGTERM)

		_ = <-sigChan
		cancel()
		if err := srv.Shutdown(); err != nil {
			log.Printf("shutdown: %v", err)
		}
	}()

	listenPort := 4140
	if len(os.Args) == 2 {
		if port, err := strconv.Atoi(os.Args[1]); err == nil {
			if 1 <= port && port <= 65535 {
				listenPort = port
			}
		}
	}

	err := srv.ListenAndServe(listenPort)
	if err != http.ErrServerClosed {
		log.Fatalf("FATAL: %v", err)
	}
}
