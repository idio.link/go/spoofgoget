# spoofgoget

Field go-get HTTP requests to enable "vanity" module names
using a custom domain. See https://go.dev/ref/mod#vcs-find
for details.
