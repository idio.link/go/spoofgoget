/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package main

import "strings"

func getLinkNext(link string) (next string) {
	// this could be made faster, but doesn't seem necessary
	// See https://www.w3.org/wiki/LinkHeader for details.
	// basic format: `<meta.rdf>; rel=meta, ...`
	before, _, found := strings.Cut(link, `rel="next"`)
	if !found {
		return
	}
	idx := strings.LastIndex(before, "<")
	if idx == -1 {
		return
	}
	r := strings.NewReader(before)
	_, err := r.ReadAt([]byte(next), int64(idx+1))
	if err != nil {
		return
	}
	parts := strings.Split(next, ">")
	if len(parts) != 2 {
		return
	}
	next = parts[0]
	return
}

type GitRepo interface {
	Path() string
	GitURI() string
	WebURI() string
	DefaultBranch() string
}
