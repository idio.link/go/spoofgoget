/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package main

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"
	"testing"
)

var srv *Server

func TestMain(m *testing.M) {
	ctx := context.Background()
	srv = NewServer(ctx)

	ret := m.Run()
	os.Exit(ret)
}

func TestGoGetModuleThatExistsOnGogs(t *testing.T) {
	module := "idio.link/go/netaddr"
	expectedStr := `<!doctype html>` +
		`<html>` +
		`<head>` +
		`<meta name="go-import"` +
		` content="idio.link/go/netaddr git https://git.idiolink.net/idio.link/netaddr.git">` +
		`<meta name="go-source"` +
		` content="idio.link/go/netaddr` +
		` https://git.idiolink.net/idio.link/netaddr` +
		` https://git.idiolink.net/idio.link/netaddr/src/master{/dir}` +
		` https://git.idiolink.net/idio.link/netaddr/src/master{/dir}/{file}#L{line}` +
		`">` +
		`</head>` +
		`<body>` +
		`go get https://idio.link/go/netaddr` +
		`</body>` +
		`</html>`
	expected := bytes.NewBufferString(expectedStr)
	reqURI := fmt.Sprintf("https://%s/v2?go-get=1", module)
	req, _ := http.NewRequest("GET", reqURI, nil)
	w := NewFakeWriter()
	srv.handleGoGet()(w, req)

	if strings.Compare(w.buf.String(), expected.String()) != 0 {
		t.Errorf("expected:\n%s\nactual:\n%s", expected.String(), w.buf.String())
	}
}

func NewFakeWriter() *FakeWriter {
	buf := make([]byte, 0, 2048)

	return &FakeWriter{
		buf: bytes.NewBuffer(buf),
	}
}

type FakeWriter struct {
	buf    *bytes.Buffer
	status int
}

func (w *FakeWriter) Header() http.Header {
	h := make(map[string][]string)

	return h
}

func (w *FakeWriter) Write(b []byte) (int, error) {
	return w.buf.Write(b)
}

func (w *FakeWriter) WriteHeader(status int) {
	w.status = status
}
