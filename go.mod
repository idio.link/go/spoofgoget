module idio.link/go/spoofgoget

go 1.20

require (
	golang.org/x/sys v0.6.0
	idio.link/go/depager/v2 v2.0.1
)
